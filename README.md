# shimanskybiz-app

English Grammar for Russian-Speakers

[![shimanskybiz-app](https://farm1.staticflickr.com/311/19836268255_4dab77cf93_o.jpg)](https://github.com/englishextra/shimanskybiz-app)

[Demo Page][]

[App Public Download Page on Phonegap.com][]

[App Public Download Page on Sourceforge.net][]

[Project's Repo on GitHub][]

  [Demo Page]: https://englishextra.github.io/webapp/
  [App Public Download Page on Phonegap.com]: https://build.phonegap.com/apps/930716/share
  [App Public Download Page on Sourceforge.net]: https://sourceforge.net/projects/shimanskybiz-app/files/
  [Project's Repo on GitHub]: https://github.com/englishextra/shimanskybiz-app
